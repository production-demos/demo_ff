package utils

import (
	"auth/models"
	"fmt"
	"os"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
)

//ConnectDB function: Make database connection
func ConnectDB() *gorm.DB {

	for _, pair := range os.Environ() {
		fmt.Println(pair)
	}
	databaseName := os.Getenv("DATABASE_NAME")
	databaseHost := os.Getenv("DATABASE_HOST")
	//gitlabEnv := os.Getenv("GITLAB_ENVIRONMENT_NAME")
	if os.Getenv("DATABASE_URL") != "" {
		databaseName = os.Getenv("DATABASE_URL")[strings.LastIndex(os.Getenv("DATABASE_URL"), "/")+1 : len(os.Getenv("DATABASE_URL"))]
		databaseHost = databaseName + "-postgresql"
	}
	//Define DB connection string
	dbURI := fmt.Sprintf("host=%s port=5432 user=user dbname=%s sslmode=disable password=testing-password", databaseHost, databaseName)

	//connect to db URI
	db, err := gorm.Open("postgres", dbURI)

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	// close db when not in use
	// defer db.Close()

	// Migrate the schema
	db.AutoMigrate(
		&models.User{})

	fmt.Println("Successfully connected!", db)
	return db
}
